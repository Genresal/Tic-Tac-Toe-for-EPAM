using NUnit.Framework;
using Tic_Tac_Toe_for_EPAM.Models;
using Tic_Tac_Toe_for_EPAM;
using System.Reflection;
using System;

namespace Tic_Tac_Toe.Tests
{
    public class Tests
    {
        private static int[,] board = new int[,] { { 11, 11, 11 },
                                    { 12, 12, 12 },
                                    { 1, 2, 0 } };
        private static GameBoard testBoard = new GameBoard(board, 3);
        private static Controller controller = new Controller(testBoard , 3);
        
        [Test]
        public void TestGameOver()
        {
            Assert.AreEqual(true, GetMethod(controller, "IsGameOver").Invoke(controller, null));
        }

        [Test]
        public void TestWinnerDetection()
        {
            Assert.AreEqual(null, GetMethod(controller, "Winner").Invoke(controller, null));
        }

        private MethodInfo GetMethod(object obj, string methodName)
        {
            if (string.IsNullOrWhiteSpace(methodName))
                Assert.Fail("methodName cannot be null or whitespace");

            var method = obj.GetType()
                .GetMethod(methodName, BindingFlags.NonPublic | BindingFlags.Instance);

            if (method == null)
                Assert.Fail(string.Format("{0} method not found", methodName));

            return method;
        }
    }
}
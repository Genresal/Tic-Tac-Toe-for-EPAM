﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Tic_Tac_Toe_for_EPAM.Models;
using Tic_Tac_Toe_for_EPAM.Helpers;

namespace Tic_Tac_Toe_for_EPAM
{
    public class Controller
    {
        private GameBoard board;
        private View view;
        private List<Player> players;
        private Dictionary<string, ConsoleColor> colors;
        private int boardSize = 3;
        Order order = Order.Random;

        private string gameOverReason;

        #region ctor
        /// <summary>
        /// Default constructor
        /// </summary>
        public Controller()
        {
            CallMainGameCycle();
        }

        /// <summary>
        /// Constructor for test
        /// </summary>
        /// <param name="board"></param>
        /// <param name="size"></param>
        public Controller(GameBoard board, int size)
        {
            this.players = new List<Player>
            {
                new HumanPlayer("Player1", Symbols.X),
                new HumanPlayer("Player2", Symbols.O)
            };
            this.boardSize = size;
            this.order = Order.Random;

            PrepareGame();

           this.board = board;
        }
        #endregion

        /// <summary>
        /// Start the game cycle
        /// </summary>
        private void CallMainGameCycle()
        {
            ViewMenu menu = new ViewMenu();

            menu.ConfugeGame(ref players, ref boardSize, ref order);
            PrepareGame();
            StartGame();
        }

        /// <summary>
        /// Prepare game
        /// </summary>
        private void PrepareGame()
        {
            foreach(Player player in players)
            {
                if (player.GetType() == typeof(HumanPlayer))
                {
                    player.MoveAction = () => GetHumanPlayerMove(player);
                }
                else
                {
                    player.MoveAction = () => GetComputerPlayerMove(player);
                }
            }

            board = new GameBoard(boardSize);

            OrderPlayers(order);

            view = new View();
            // colors for symbols
            colors = new Dictionary<string, ConsoleColor>();
            colors.Add("X", ConsoleColor.Red);
            colors.Add("O", ConsoleColor.Yellow);
            colors.Add("x", ConsoleColor.DarkRed);
            colors.Add("o", ConsoleColor.DarkYellow);
        }

        /// <summary>
        /// Start game
        /// </summary>
        private void StartGame()
        {
            bool gameOver = false;
            PrintTable(board);
            GetPossibleCombinations();
            do
            {
                foreach (var player in players)
                {

                    gameOver = IsGameOver();
                    if (gameOver)
                    {
                        break;
                    }
                    
                    player.Move(board);

                    board.AddMove(player.ActualMove, player.Symbol);
                  
                    player.Scope += board.GetCombinations(player.ActualMove, player.Symbol);

                    PrintTable(board);
                }
            } while (!gameOver);

            view.PrintGameOver(gameOverReason, players);
            view.PrintWinner(Winner());

            CallMainGameCycle();
        }

        /// <summary>
        /// Get human move result
        /// </summary>
        /// <param name="player">The player instance</param>
        private void GetHumanPlayerMove(Player player)
        {
            int position = 0;
            while (position == 0)
            {
                if (int.TryParse(view.PrintPlayerMoveRequest(player), out position))
                {
                    if (board.IsMovePositionValid(position) && board.IsPositionFree(position))
                    {
                        break;
                    }
                    position = 0;
                }
                view.PrintIncorrectInputWarning();
            }

            player.ActualMove = position;
        }

        /// <summary>
        /// Get computer move result
        /// </summary>
        /// <param name="player">The player instance</param>
        private void GetComputerPlayerMove(Player player)
        {
            view.PrintCurrentPlayerInfo(player);
        }

        #region Aux methods
        /// <summary>
        /// Sort player list
        /// </summary>
        /// <param name="order"></param>
        private void OrderPlayers(Order order)
        {
            if (order == Order.Random)
            {
                Random rnd = new Random();
                for (int i = 1; i <= rnd.Next(1, 5); i++)
                {
                    players.Reverse();
                }
                return;
            }

            if (players[0].Name == order.ToString())
            {
                return;
            }

            players.Reverse();
        }

        /// <summary>
        /// Get possible combinations for the actual board stage
        /// </summary>
        private void GetPossibleCombinations()
        {
            foreach (var player in players)
            {
                GameBoard boardClone = (GameBoard)board.Clone();
                player.PossibleCombinations = boardClone.GetPossiblesCombinations(player.Symbol);
            }
        }

        /// <summary>
        /// Check if it is time to finish the game
        /// </summary>
        /// <returns>True if need to finish the game</returns>
        private bool IsGameOver()
        {
            GetPossibleCombinations();

            if (players.Sum(x => x.PossibleCombinations) == 0)
            {
                gameOverReason = "No more possible combinations";
                return true;
            }

            if (players[0].Scope > players[1].Scope + players[1].PossibleCombinations)
            {
                gameOverReason = players[0].Name + " has scope more than another player can get";
                return true;
            }

            if (players[1].Scope > players[0].Scope + players[0].PossibleCombinations)
            {
                gameOverReason = players[1].Name + " has scope more than another player can get";
                return true;
            }
            return false;
        }

        /// <summary>
        /// Return winner, if return null - no winner
        /// </summary>
        /// <returns></returns>
        private Player Winner()
        {
            _ = players.OrderByDescending(x => x.Scope);
            if (players[0].Scope == players[1].Scope)
            {
                return null;
            }

            return players[0];
        }

        /// <summary>
        /// Print the actual game stage representation
        /// </summary>
        /// <param name="board">The actual game board</param>
        private void PrintTable(GameBoard board)
        {
            view.PrintTable(board.ToStringArray(), colors);
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Tic_Tac_Toe_for_EPAM.Models
{
    public enum Symbols
    {
        Empty,
        X,
        O,
        // Vars for marking fields which used in combinations
        empty = 10,
        x,
        o
    };

    public enum Directions
    {
        Normal = -1,
        Null,
        Inverted
    };

    public class GameBoard : ICloneable
    {
        private int boardSize;
        private int[,] gameBoard;
        #region ctor
        /// <summary>
        /// Create new empty board
        /// </summary>
        /// <param name="size">The size of the game board</param>
        public GameBoard(int size)
        {
            boardSize = size;
            gameBoard = new int[boardSize, boardSize];
        }

        /// <summary>
        /// Create a board from exsisting exsisting one
        /// </summary>
        /// <param name="newGameBoard">Exsisting board array</param>
        /// <param name="size">The size of the game board</param>
        public GameBoard(int[,] newGameBoard, int size) : this(size)
        {
            for (int i = 0; i < size; i++)
            {
                for (int q = 0; q < size; q++)
                {
                    gameBoard[i, q] = newGameBoard[i, q];
                }
            }
        }
        #endregion

        #region ICloneable
        public object Clone()
        {
            GameBoard b = new GameBoard(gameBoard, boardSize);
            return b;
        }

        #endregion

        /// <summary>
        /// Add move to the board
        /// </summary>
        /// <param name="position">The move position</param>
        /// <param name="symbol">The symbol which need to add</param>
        public void AddMove(int position, Symbols symbol)
        {
            Point point = PositionToPoint(position);

            gameBoard[point.X, point.Y] = (int)symbol;
        }

        /// <summary>
        /// Convert int array to string array
        /// </summary>
        /// <returns>View representation of the game board</returns>
        public string[,] ToStringArray()
        {
            string[,] result = new string[boardSize, boardSize];

            for (int i = 0; i < boardSize; i++)
            {
                for (int q = 0; q < boardSize; q++)
                {

                    if (gameBoard[i, q] == (int)Symbols.Empty)
                    {
                        result[i, q] = (i * boardSize + (q + 1)).ToString();
                    }
                    else
                    {
                        result[i, q] = ((Symbols)gameBoard[i, q]).ToString();
                    }
                }
            }

            return result;
        }

        #region Aux methods

        /// <summary>
        /// Check the position
        /// </summary>
        /// <param name="position">Sequence number of position</param>
        /// <returns>True if position on board</returns>
        public bool IsMovePositionValid(int position)
        {
            int maxPositionNumber = boardSize * boardSize;
            return (position <= maxPositionNumber && position >= 1);
        }

        /// <summary>
        /// Check the position occupation
        /// </summary>
        /// <param name="position">Sequence number of position</param>
        /// <returns>True if not occupied</returns>
        public bool IsPositionFree(int position)
        {
            Point point = PositionToPoint(position);
            return IsPositionFree(point);
        }

        /// <summary>
        /// Check the position occupation
        /// </summary>
        /// <param name="point">The point which represent position</param>
        /// <returns>True if not occupied</returns>
        private bool IsPositionFree(Point point)
        {
            return gameBoard[point.X, point.Y] == 0;
        }

        /// <summary>
        /// Check the point is valid
        /// </summary>
        /// <param name="point">The point which represent position</param>
        /// <returns>True if point inside the game board</returns>
        private bool IsPointOnBoard(Point point)
        {
            return point.X < boardSize && point.Y < boardSize && point.X >= 0 && point.Y >= 0;
        }

        /// <summary>
        /// Convert the position number to the point
        /// </summary>
        /// <param name="position">Sequence number of position</param>
        /// <returns></returns>
        private Point PositionToPoint(int position)
        {
            Point point = new Point();
            point.X = (position - 1) / boardSize;
            point.Y = (position - 1) % boardSize;
            return point;
        }

        /// <summary>
        /// Convert the point to position
        /// </summary>
        /// <param name="point">The point which represent position</param>
        /// <returns></returns>
        public int GetPositionFromPoint(Point point)
        {
            return point.X * boardSize + point.Y + 1;
        }

        /// <summary>
        /// Get field status
        /// </summary>
        /// <param name="point">The point which represent position</param>
        /// <returns></returns>
        private int GetField(Point point)
        {
            return gameBoard[point.X, point.Y];
        }

        /// <summary>
        /// Get field status
        /// </summary>
        /// <param name="point">The point which represent position</param>
        /// <returns></returns>
        private Symbols GetFieldStatus(Point point)
        {
            if (IsPositionFree(point))
            {
                return Symbols.Empty;
            }

            return (Symbols)Enum.ToObject(typeof(Symbols), GetField(point));
        }

        #endregion

        #region Board checking methods

        /// <summary>
        /// Set all positions with specific symbol to empty
        /// </summary>
        /// <param name="symbol">The symbol which represent a player</param>
        public void ReplaceSymbolToEmpty(Symbols symbol)
        {
            for (int i = 0; i < boardSize; i++)
            {
                for (int q = 0; q < boardSize; q++)
                {
                    if (gameBoard[i, q] == (int)symbol)
                    {
                        gameBoard[i, q] = (int)Symbols.Empty;
                    }
                }
            }
        }

        /// <summary>
        /// Count possible combinations for specified symbol
        /// </summary>
        /// <param name="symbol">The symbol which represent a player</param>
        /// <returns>Number of possibles combinations on the game board</returns>
        public int GetPossiblesCombinations(Symbols symbol)
        {
            int result = 0;
            ReplaceSymbolToEmpty(symbol);
            List<Point> freePoints = GetFreePoints();

            foreach (Point point in freePoints)
            {
                if (IsPointOnBoard(point) && IsPositionFree(point))
                {
                    result += GetCombinations(point);
                }
            }
            return result;
        }

        /// <summary>
        /// Count free positions on the game board
        /// </summary>
        /// <returns>Numbers of free positions</returns>
        public int CountFreePositions()
        {
            return GetFreePoints().Count;
        }

        /// <summary>
        /// Return the list of free points on the game board
        /// </summary>
        /// <returns></returns>
        public List<Point> GetFreePoints()
        {
            List<Point> points = new List<Point>();

            for (int i = 0; i < boardSize; i++)
            {
                for (int q = 0; q < boardSize; q++)
                {
                    Point point = new Point(i, q);
                    if (IsPositionFree(point))
                    {
                        points.Add(point);
                    }
                }
            }
            return points;
        }

        /// <summary>
        /// Get possibles combinations for the specifiс position
        /// </summary>
        /// <param name="position">Sequence number of position</param>
        /// <param name="symbol">The symbol which represent a player</param>
        /// <returns></returns>
        public int GetCombinations(int position, Symbols symbol = Symbols.Empty)
        {
            return GetCombinations(PositionToPoint(position), symbol);
        }

        /// <summary>
        /// Get possibles combinations for the specifiс position
        /// Check 4 lines - horizontal, vertical and 2 diagonals
        /// If the combination is located on the line add 1 to count
        /// </summary>
        /// <param name="point">The point which represent position</param>
        /// <param name="symbol">The symbol which represent a player</param>
        /// <returns></returns>
        private int GetCombinations(Point point, Symbols symbol = Symbols.Empty)
        {
            List<Point> pointsForChange = new List<Point>();
            int result = 0;

            if (IsCombinationOnX(point, ref pointsForChange))
                result++;

            if (IsCombinationOnY(point, ref pointsForChange))
                result++;

            if (IsCombinationOnLeftToRight(point, ref pointsForChange))
                result++;

            if (IsCombinationOnRightToLeft(point, ref pointsForChange))
                result++;
            //mark fields which was in a combination
            foreach (var item in pointsForChange)
            {
                gameBoard[item.X, item.Y] = (int)symbol + 10;
            }

            return result;
        }

        /// <summary>
        /// Compare two points
        /// </summary>
        /// <param name="points">The list of points what represent position</param>
        /// <returns>True if points are equal</returns>
        private bool IsPointsEqual(List<Point> points)
        {
            if (points.Any(x => !IsPointOnBoard(x)))
            {
                return false;
            }

            var symbol = GetFieldStatus(points.First());

            if (points.All(x => GetFieldStatus(x) == symbol))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Check combinations on the horisontal line
        /// </summary>
        /// <param name="point">The point which represent position</param>
        /// <param name="pointsForChange">The list of points what represent position</param>
        /// <returns></returns>
        private bool IsCombinationOnX(Point point, ref List<Point> pointsForChange)
        {
            return IsCombinationOnLine(point, ref pointsForChange, Directions.Normal, Directions.Null);
        }

        /// <summary>
        /// Check combinations on the vertical line
        /// </summary>
        /// <param name="point">The point which represent position</param>
        /// <param name="pointsForChange">The list of points what represent position</param>
        /// <returns></returns>
        private bool IsCombinationOnY(Point point, ref List<Point> pointsForChange)
        {
            return IsCombinationOnLine(point, ref pointsForChange, Directions.Null, Directions.Normal);
        }

        /// <summary>
        /// Check combinations on the first diagonal line
        /// </summary>
        /// <param name="point">The point which represent position</param>
        /// <param name="pointsForChange">The list of points what represent position</param>
        /// <returns></returns>
        private bool IsCombinationOnLeftToRight(Point point, ref List<Point> pointsForChange)
        {
            return IsCombinationOnLine(point, ref pointsForChange, Directions.Normal, Directions.Normal);
        }

        /// <summary>
        /// Check combinations on the second diagonal line
        /// </summary>
        /// <param name="point">The point which represent position</param>
        /// <param name="pointsForChange">The list of points what represent position</param>
        /// <returns></returns>
        private bool IsCombinationOnRightToLeft(Point point, ref List<Point> pointsForChange)
        {
            return IsCombinationOnLine(point, ref pointsForChange, Directions.Normal, Directions.Inverted);
        }

        /// <summary>
        /// Check combinations on the line
        /// [^][^][^][][]
        /// [][^][^][^][]
        /// [][][^][^][^]
        /// </summary>
        /// <param name="point">The point which represent position</param>
        /// <param name="pointsForChange"></param>
        /// <param name="directionX"></param>
        /// <param name="directionY"></param>
        /// <returns>True if combination found</returns>
        private bool IsCombinationOnLine(Point point, ref List<Point> pointsForChange, Directions directionX, Directions directionY)
        {
            List<Point> leftList = new List<Point>
            {
                new Point(point.X - 2 * (int)directionX, point.Y - 2 * (int)directionY),
                new Point(point.X - 1 * (int)directionX, point.Y - 1 * (int)directionY),
                point
            };

            List<Point> middleList = new List<Point>
            {
                new Point(point.X - 1 * (int)directionX, point.Y - 1 * (int)directionY),
                point,
                new Point(point.X + 1 * (int)directionX, point.Y + 1 * (int)directionY)
            };

            List<Point> rightList = new List<Point>
            {
                point,
                new Point(point.X + 1 * (int)directionX, point.Y + 1 * (int)directionY),
                new Point(point.X + 2 * (int)directionX, point.Y + 2 * (int)directionY),
            };

            if (IsPointsEqual(middleList))
            {
                pointsForChange.AddRange(middleList);
                return true;
            }

            if (IsPointsEqual(leftList))
            {
                pointsForChange.AddRange(leftList);
                return true;
            }

            if (IsPointsEqual(rightList))
            {
                pointsForChange.AddRange(rightList);
                return true;
            }

            return false;
        }

        #endregion
    }
}

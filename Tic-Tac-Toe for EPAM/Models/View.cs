﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tic_Tac_Toe_for_EPAM.Models;

namespace Tic_Tac_Toe_for_EPAM.Helpers
{
    public class View
    {
        /// <summary>
        /// Print to console "GAME OVER", the reason why the game ends and players final scope
        /// </summary>
        /// <param name="gameOverReason">The reason string</param>
        /// <param name="players">The list of players</param>
        public void PrintGameOver(string gameOverReason, List<Player> players)
        {
            Console.WriteLine("GAME OVER!");
            Console.WriteLine(gameOverReason);
            foreach(Player player in players)
            {
                Console.WriteLine(player.Name + " final scope: " + player.Scope.ToString());
            }
        }

        /// <summary>
        /// Print to console the result of the game
        /// </summary>
        /// <param name="winner">The player who wins the game, if it null - then we have draw</param>
        public void PrintWinner(Player winner)
        {
            if (winner == null)
            {
                Console.WriteLine("It is a draw...");
            }
            else
            {
                Console.WriteLine("The winner is " + winner.Name + "!");
            }
            Console.WriteLine("\r\nPress any key to go to menu");
            Console.ReadKey();
        }

        /// <summary>
        /// Print to console the player input request and return inputed string
        /// </summary>
        /// <param name="player">The current player</param>
        /// <returns></returns>
        public string PrintPlayerMoveRequest(Player player)
        {
            PrintCurrentPlayerInfo(player);
            Console.WriteLine("Input prefer cell number:");
            return Console.ReadLine();
        }

        /// <summary>
        /// Print to console a player scope and possible combinations
        /// </summary>
        /// <param name="player">The current player</param>
        public void PrintCurrentPlayerInfo(Player player)
        {
            Console.WriteLine(player.Name + " scope: " + player.Scope.ToString());
            Console.WriteLine(player.Name + " possibles combinations: " + player.PossibleCombinations.ToString());
            Console.WriteLine("It is " + player.Name + " turn:");
        }

        /// <summary>
        /// Print to console the current state of the game
        /// </summary>
        /// <param name="viewBoard">The current game board</param>
        /// <param name="colors">The dictionary with colors of symbols</param>
        public void PrintTable(string[,] viewBoard, Dictionary<string, ConsoleColor> colors)
        {
            int arrayLength = viewBoard.Length;
            //Get size from the array length
            int size = (int)Math.Sqrt(arrayLength);
            //Calculate cell size by conting maximum position in the array
            int cellWight = arrayLength.ToString().Length + 2;

            if (size < 3)
            {
                Console.WriteLine("Cant draw table, something went wrong");
            }
            Console.WriteLine("");

            PrintSpacer(cellWight * size + size+1);

            PrintHorizontalLine(cellWight, size);
            for (int i = 0; i < size; i++)
            {
                for (int q = 0; q < size; q++)
                    {
                    Console.Write("|");
                    Console.Write(" ");
                    if (colors.Any(x => x.Key == viewBoard[i, q]))
                    {
                        Console.ForegroundColor = colors.FirstOrDefault(x => x.Key == viewBoard[i, q]).Value;
                    }
                    else
                    {
                        //Spare positions color
                        Console.ForegroundColor = ConsoleColor.DarkGray;
                    }
                    Console.Write(ManageSpaces(viewBoard[i, q], arrayLength));
                    Console.ResetColor();
                    Console.Write(" ");
                }
                Console.WriteLine("|");
                PrintHorizontalLine(cellWight, size);
            }
            Console.WriteLine("");
        }

        /// <summary>
        /// Print to console warning for incorrect input case
        /// </summary>
        public void PrintIncorrectInputWarning()
        {
            Console.WriteLine("Incorrect input, please try again");
        }

        #region Aux methods
        /// <summary>
        /// Return string filled by extra spaces 
        /// </summary>
        /// <param name="str">The string what need to fill</param>
        /// <param name="targetLength">Desired string length</param>
        /// <returns></returns>
        string ManageSpaces(string str, int targetLength)
        {
            if (str.Length < targetLength.ToString().Length)
            {
                int numOfSpaces = targetLength.ToString().Length;
                return string.Format("{0," + numOfSpaces.ToString() + "}", str);
            }
            return str;
        }

        /// <summary>
        /// Print to console a horisontal line for the game board
        /// </summary>
        /// <param name="cellWight">Size of the cell</param>
        /// <param name="columnsCount">The number of columns</param>
        void PrintHorizontalLine(int cellWight, int columnsCount)
        {
            for (int i = 0; i < columnsCount; i++)
            {
                Console.Write("+");
                Console.Write(new string('-', cellWight));
            }
            Console.WriteLine("+");
        }

        /// <summary>
        /// Print to console number signs line
        /// </summary>
        /// <param name="length">Desired string length</param>
        void PrintSpacer(int length)
        {
            Console.WriteLine(new string('#', length));
            Console.WriteLine("");
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tic_Tac_Toe_for_EPAM.Models
{
    class MenuItem
    {
        #region ctor
        /// <summary>
        /// Menu item constructor
        /// </summary>
        /// <param name="name">The menu item text</param>
        /// <param name="selected">The delegate what will be called</param>
        public MenuItem(string name, Action selected)
        {
            Name = name;
            Selected = selected;
        }
        #endregion

        public string Name { get; set; }
        public Action Selected { get; set; }
    }
}

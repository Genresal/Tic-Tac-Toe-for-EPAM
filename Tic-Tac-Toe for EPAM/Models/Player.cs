﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tic_Tac_Toe_for_EPAM.Models
{
    public abstract class Player
    {
        #region ctor
        /// <summary>
        /// Player constructor
        /// </summary>
        /// <param name="name">The player's name</param>
        /// <param name="symbol">The choosed symbol</param>
        protected Player(string name, Symbols symbol)
        {
            Name = name;
            Symbol = symbol;
        }
        #endregion

        public int ActualMove { get; set; }
        public Action MoveAction { get; set; }
        public string Name { get; set; }
        public Symbols Symbol { get; set; }
        public int Scope { get; set; }
        public int PossibleCombinations { get; set; }

        public abstract void Move(GameBoard board);
    }

    public class HumanPlayer : Player
    {
        #region ctor
        /// <summary>
        /// Human player constructor
        /// </summary>
        /// <param name="name">The player's name</param>
        /// <param name="symbol">The choosed symbol</param>
        public HumanPlayer(string name, Symbols symbol) :base(name, symbol)
        {
        }
        #endregion

        /// <summary>
        /// Call the move delegate
        /// </summary>
        /// <param name="board"></param>
        public override void Move(GameBoard board)
        {
            MoveAction();
        }
    }

    public class ComputerPlayer : Player
    {
        /// <summary>
        /// Computer player constructor
        /// </summary>
        /// <param name="name">The player's name</param>
        /// <param name="symbol">The choosed symbol</param>
        public ComputerPlayer(string name, Symbols symbol) : base(name, symbol)
        {
        }

        /// <summary>
        /// Calculate the computer move then all the move delegate
        /// </summary>
        /// <param name="board"></param>
        public override void Move(GameBoard board)
        {
            List<Point> freePoints = board.GetFreePoints();

            Random r = new Random();
            Point targetPoint = freePoints[r.Next(1, freePoints.Count)];

            ActualMove = board.GetPositionFromPoint(targetPoint);

            MoveAction();
        }
    }
}

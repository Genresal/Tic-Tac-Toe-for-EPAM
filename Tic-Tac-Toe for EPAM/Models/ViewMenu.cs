﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tic_Tac_Toe_for_EPAM.Models
{
    public enum Order { Random = -1, Player1, Player2, End }

    class ViewMenu
    {
        const int maxBoardSize = 15;

        protected List<MenuItem> selectedMenu;
        public List<MenuItem> mainMenu;
        public List<MenuItem> playerMenu;

        List<Player> players;   //Actual players list

        public int boardSize;
        public Order selectedOrder;

        int menuIndex = 0;

        bool startGame = false;

        /// <summary>
        /// Call menu and get player options
        /// </summary>
        /// <param name="players">List of players</param>
        /// <param name="boardSize">Selected the game board size</param>
        /// <param name="selectedOrder">Selected game order</param>
        public void ConfugeGame(ref List<Player> players, ref int boardSize, ref Order selectedOrder)
        {
            this.boardSize = boardSize;
            this.selectedOrder = selectedOrder;
            Menu();
            players = this.players;
            boardSize = this.boardSize;
            selectedOrder = this.selectedOrder;
        }

        /// <summary>
        /// Manage menus
        /// </summary>
        void Menu()
        {
            PrepareMainMenu();
            selectedMenu = mainMenu;
            PrintMenu(selectedMenu, menuIndex);

            ConsoleKeyInfo keyInfo;

            do
            {
                if(startGame)
                {
                    break;
                }

                keyInfo = Console.ReadKey();

                if (keyInfo.Key == ConsoleKey.DownArrow && menuIndex + 1 < selectedMenu.Count)
                {
                    menuIndex++;
                    PrintMenu(selectedMenu, menuIndex);
                }
                if (keyInfo.Key == ConsoleKey.UpArrow && menuIndex - 1 >= 0)
                {
                    menuIndex--;
                    PrintMenu(selectedMenu, menuIndex);
                }

                if (keyInfo.Key == ConsoleKey.Enter)
                {
                    selectedMenu[menuIndex].Selected.Invoke();
                }
            }
            while (keyInfo.Key != ConsoleKey.X);
        }

        /// <summary>
        /// Print to console the current menu
        /// </summary>
        /// <param name="menu">The current menu</param>
        /// <param name="index">Menus selector</param>
        void PrintMenu(List<MenuItem> menu, int index)
        {
            selectedMenu = menu;
            var selectedItem = selectedMenu[index];
            Console.Clear();
            foreach (MenuItem item in menu)
            {
                if (item == selectedItem)
                {
                    Console.Write("> ");
                }
                else
                {
                    Console.Write("  ");
                }

                Console.WriteLine(item.Name);
            }
        }

        #region Main menu items
        /// <summary>
        /// Set main menu items
        /// </summary>
        void PrepareMainMenu()
        {
            mainMenu = new List<MenuItem>
            {
                new MenuItem("PLAYER VS COMPUTER", () => PlayerWithComputerSelected()),
                new MenuItem("2 PLAYERS", () => HumanPlayersSelected()),
                new MenuItem("EXIT", () => Environment.Exit(0))
            };
        }

        /// <summary>
        /// Add to player list a human player and a computer player then print menu
        /// </summary>
        void PlayerWithComputerSelected()
        {
            players = new List<Player> {
                new HumanPlayer("Player1", Symbols.X),
                new ComputerPlayer("Computer1", Symbols.O)
            };

            PreparePlayerMenu();

            menuIndex = 0;
            PrintMenu(playerMenu, 0);
        }

        /// <summary>
        /// Add to player list 2 human players then print menu
        /// </summary>
        void HumanPlayersSelected()
        {
            players = new List<Player> {
                new HumanPlayer("Player1", Symbols.X),
                new HumanPlayer("Player2", Symbols.O)
            };

            PreparePlayerMenu();

            menuIndex = 0;
            PrintMenu(playerMenu, 0);
        }
        #endregion

        #region Player menu items

        /// <summary>
        /// Set player menu items
        /// </summary>
        void PreparePlayerMenu()
        {
            string playerSymbol = players.First(x => x.Name == "Player1").Symbol.ToString();

            playerMenu = new List<MenuItem>
            {
                new MenuItem("START", () => StartGame()),
                new MenuItem("SELECT BOARD SIZE: " + boardSize.ToString(), () => SelectBoardSize()),
                new MenuItem("FIRST MOVE: " + selectedOrder.ToString(), () => SelectPlayersOrder()),
                new MenuItem("CHOOSE SYMBOLS: PLAYER1 - " +  playerSymbol, () => SelectFirstPlayerSymbol()),
                new MenuItem("BACK", () => Back())
            };
        }

        /// <summary>
        /// Print to console the gameboard size requirements and manage inputed value
        /// </summary>
        void SelectBoardSize()
        {
            Console.Clear();
            Console.WriteLine("INPUT GAME BOARD SIZE, IT IS MUST BE AN ODD NUMBER FROM 3 TO 15");

            do
            {
                string input = Console.ReadLine();
                if (int.TryParse(input, out boardSize) && IsBoardSizeValid(boardSize))
                {
                    playerMenu[1].Name = "SELECT BOARD SIZE: " + boardSize.ToString();
                    PrintMenu(playerMenu, 1);
                    break;
                }
                Console.WriteLine("The game board size is invalid. It must be greater or equal 3 and be an odd number");
                Console.WriteLine("SELECT GAME BOARD SIZE, IT IS MUST BE FROM 3 TO " + maxBoardSize.ToString());
            }
            while (true);
        }

        /// <summary>
        /// Set next item from the enum then print the menu
        /// </summary>
        void SelectPlayersOrder()
        {
            selectedOrder = (Order)((int)selectedOrder + 1);
            if (selectedOrder == Order.End)
            {
                selectedOrder = Order.Random;
            }

            PreparePlayerMenu();
            PrintMenu(playerMenu, 2);
        }

        /// <summary>
        /// Switch first player game symbol
        /// </summary>
        void SelectFirstPlayerSymbol()
        {
            Symbols symbolBuffer = players[0].Symbol;
            players[0].Symbol = players[1].Symbol;
            players[1].Symbol = symbolBuffer;

            PreparePlayerMenu();
            PrintMenu(playerMenu, 3);
        }

        /// <summary>
        /// Set start game bit
        /// </summary>
        void StartGame()
        {
            Console.Clear();
            startGame = true;
        }

        /// <summary>
        /// Set the main menu as actual
        /// </summary>
        void Back()
        {
            menuIndex = 0;
            PrintMenu(mainMenu, 0);
        }

        #endregion

        #region AUX method

        /// <summary>
        /// Check entered game board size
        /// </summary>
        /// <param name="size"></param>
        /// <returns>If value valid return true</returns>
        bool IsBoardSizeValid(int size)
        {
            return (size >= 3 && size <= maxBoardSize && size % 2 != 0);
        }

        #endregion
    }
}


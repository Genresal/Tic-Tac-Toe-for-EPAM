﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tic_Tac_Toe_for_EPAM.Models
{
    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }

        #region ctor
        /// <summary>
        /// Construct a new point, default constructor
        /// </summary>
        public Point()
        { }

        /// <summary>
        /// Construct a new point
        /// </summary>
        /// <param name="x">The row coordinate</param>
        /// <param name="y">The column coordinate</param>
        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }
        #endregion
    }
}
